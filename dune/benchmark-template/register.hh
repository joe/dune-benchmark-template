#ifndef DUNE_BENCHMARK_TEMPLATE_REGISTER_HH
#define DUNE_BENCHMARK_TEMPLATE_REGISTER_HH

#include <array>
#include <cstddef>
#include <functional>
#include <map>
#include <string>
#include <utility>

#include <benchmark/benchmark.h>

#include <dune/common/exceptions.hh>

#include <dune/benchmark-template/sequencetools.hh>

namespace Dune {
  namespace BenchmarkTemplate {

    namespace RegisterTemplateImpl {
      template<class Benchmark, std::size_t argCount>
      class TemplateBenchmark {
        Benchmark benchmark_;

        using Key = std::array<int, argCount>;
        using Value = std::function<void(benchmark::State &)>;

        std::map<Key, Value> callTable_;

      public:
        template<class B, class... ArgRanges>
        TemplateBenchmark(B &&benchmark, ArgRanges...) :
          benchmark_(std::forward<B>(benchmark))
        {
          static_assert(argCount == sizeof...(ArgRanges), "");
          for_each<ArgRanges...>([this] (auto... args) {
              callTable_[{int(decltype(args)::value)...}] =
                [this,args...] (benchmark::State &state)
                {
                  benchmark_(state, args...);
                };
            });
        }

        void operator()(benchmark::State &state) const
        {
          Key key;
          for(std::size_t i = 0; i < argCount; ++i)
            key[i] = state.range(i);
          auto i = callTable_.find(key);
          if(i == callTable_.end()) {
            std::string keystr;
            const char *sep = "";
            for(auto i : key) {
              keystr += sep + std::to_string(i);
              sep = ", ";
            }
            DUNE_THROW(Dune::Exception, "benchmark called with an unprepared "
                       << "combination of arguments {" << keystr << "}");
          }
          i->second(state);
        }
      };

    } // namespace RegisterTemplateImpl

    template<class Benchmark, class... ArgRanges>
    auto registerTemplate(std::string name, Benchmark &&benchmark,
                          ArgRanges...)
    {
      RegisterTemplateImpl::TemplateBenchmark<Benchmark, sizeof...(ArgRanges)>
        tb(std::forward<Benchmark>(benchmark), ArgRanges()...);
      auto p = benchmark::RegisterBenchmark(name.c_str(), std::move(tb));

      for_each<ArgRanges...>([p] (auto... args) {
          p->Args({int(decltype(args)::value)...});
        });
      return p;
    }

  } // namespace BenchmarkTemplate
} // namespace Dune

#endif // DUNE_BENCHMARK_TEMPLATE_REGISTER_HH
