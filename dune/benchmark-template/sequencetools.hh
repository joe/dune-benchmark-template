#ifndef DUNE_BENCHMARK_TEMPLATE_SEQUENCETOOLS_HH
#define DUNE_BENCHMARK_TEMPLATE_SEQUENCETOOLS_HH

#include <cstddef>
#include <tuple>
#include <type_traits>
#include <utility>

namespace Dune {
  namespace BenchmarkTemplate {

    namespace CatSequenceImpl {
      template<class... Seq>
      struct cat_sequence;

      template<class I, I... i0s>
      struct cat_sequence<std::integer_sequence<I, i0s...> >
      {
        using type = std::integer_sequence<I, i0s...>;
      };

      template<class I, I... i0s, I... i1s>
      struct cat_sequence<std::integer_sequence<I, i0s...>,
                          std::integer_sequence<I, i1s...> >
      {
        using type = std::integer_sequence<I, i0s..., i1s...>;
      };

      template<class Seq0, class... Tail>
      struct cat_sequence<Seq0, Tail...>
      {
        using type =
          typename cat_sequence<Seq0,
                                typename cat_sequence<Tail...>::type>::type;
      };
    } // namespace CatSequenceImpl

    //! concatenate compatible integer sequences into one
    template<class... Seq>
    using cat_sequence = typename CatSequenceImpl::cat_sequence<Seq...>::type;

    //! namespace for generators
    /**
     * Generators must fulfill the following interface:
     * <ul>
     * <li>They must be of literal type.</li>
     * <li>They must contain a member type \c value_type.</li>
     * <li>Unless they are passed-the-end, they must contain a \c static \c
     *     constexpr member constant \c value with type \c value_type.</li>
     * <li>Unless they are passed-the-end, they must contain a member type \c
     *     next, which yields the next generator.</li>
     * <li>Two generators \c G1, \G2 of the same domain must be comparable \c
     *     G1()==G2() and the result must be a constant expression.</li>
     * </ul>
     */
    namespace Generator {

      //! Generate values by incrementing with a constant
      template<class I, I v, I by = 1>
      struct increment {
        using value_type = I;
        static constexpr auto value = v;
        using next = increment<I, v+by, by>;
        template<I v2>
        constexpr bool operator==(increment<I, v2, by>) const
        {
          return v == v2;
        }
      };

      //! Abbreviation of \c increment for \c std::size_t
      template<std::size_t v, std::size_t by = 1>
      using iincrement = increment<std::size_t, v, by>;

      //! Generate values by multiplying with a constant
      template<class I, I v, I by>
      struct multiply {
        using value_type = I;
        static constexpr auto value = v;
        using next = multiply<I, v*by, by>;
        template<I v2>
        constexpr bool operator==(multiply<I, v2, by>) const
        {
          return v == v2;
        }
      };

      //! Abbreviation of \c multiply for \c std::size_t
      template<std::size_t v, std::size_t by>
      using imultiply = multiply<std::size_t, v, by>;

    } // namespace Generator

    //! namespace for sentinels
    /**
     * Sentinels must fulfill the following interface:
     * <ul>
     * <li>They must be of literal type.</li>

     * <li>A sentinel \c S must be comparable with a generator \c G: \c
     *     G()==S(), and the result must be a constant expression.  Note that
     *     the symmetric operation \c S()==G() is not necessarily supported.
     *     Note also that the sentinel may not support comparison with
     *     passed-the-end generators.</li>
     * <li>When a sentinel contains a member type \c next_sentinel, that type
     *     denotes the sentinel to use for the next iteration when generating,
     *     otherwise the sentinel is reused.  (This is needed to support the
     *     \c size sentinel.)</li>
     * </ul>
     */
    namespace Sentinel {

      //! stop after generating \c i elements
      /**
       * May be compared to passed-the-end generators.
       */
      template<std::size_t i>
      struct size {
        using next_sentinel = size<i-1>;
        template<class Generator>
        friend constexpr bool operator==(Generator, size)
        {
          return i == 0;
        }
      };

      //! stop when the next element would be \c >v
      /**
       * Must not be compared to passed-the-end generators.
       */
      template<class I, I v>
      struct greater {
        template<class Generator>
        friend constexpr bool operator==(Generator, greater)
        {
          return Generator::value > v;
        }
      };

      //! Abbreviation of \c greater for \c std::size_t
      template<std::size_t v>
      using igreater = greater<std::size_t, v>;

      //! stop when the next element would be \c >=v
      /**
       * Must not be compared to passed-the-end generators.
       */
      template<class I, I v>
      struct greater_equal {
        template<class Generator>
        friend constexpr bool operator==(Generator, greater_equal)
        {
          return Generator::value >= v;
        }
      };

      //! Abbreviation of \c greater_equal for \c std::size_t
      template<std::size_t v>
      using igreater_equal = greater_equal<std::size_t, v>;

    } // namespace Sentinel

    namespace NextSentinelImpl {

      template<class S, class = void>
      struct next {
        using type = S;
      };

      template<class S>
      struct next<S, std::enable_if_t<sizeof(typename S::next_sentinel)> >
      {
        using type = typename S::next_sentinel;
      };

    } // namespace NextSentinelImpl

    //! get the next sentinel while iterating
    /**
     * This yields either \c Sentinel::next_sentinel if that exists and is a
     * type, or otherwise \c Sentinel.
     */
    template<class Sentinel>
    using next_sentinel = typename NextSentinelImpl::next<Sentinel>::type;

    namespace GenerateIntegerSequenceImpl {

      template<class I, class Generator, class Sentinel, class = void>
      struct generate {
        using type = cat_sequence<
          std::integer_sequence<I, Generator::value>,
          typename generate<I, typename Generator::next,
                            next_sentinel<Sentinel> >::type>;
      };

      template<class I, class Generator, class Sentinel>
      struct generate<I, Generator, Sentinel,
                      std::enable_if_t<Generator() == Sentinel()> >
      {
        using type = std::integer_sequence<I>;
      };
    } // namespace GenerateIntegerSequenceImpl

    //! generate an integer sequence
    /**
     * Generate an integer sequence from \c Generator, stopping when
     * comparison with \c Sentinel returns \c true.
     */
    template<class Generator, class Sentinel>
    using generate_sequence =
      typename GenerateIntegerSequenceImpl::generate
      <typename Generator::value_type, Generator, Sentinel>::type;

    //! make an integer range
    template<class I, I begin, I end>
    using make_integer_range =
      generate_sequence<Generator::increment<I, begin>,
                        Sentinel::greater_equal<I, end> >;

    //! make an index range
    template<std::size_t begin, std::size_t end>
    using make_index_range = make_integer_range<std::size_t, begin, end>;

    namespace ForEachWhileImpl {
      template<class I, class Func>
      constexpr bool for_each_while(std::integer_sequence<I>, Func &&func)
      {
        return true;
      }

      template<class I, I head, I... tail, class Func>
      constexpr bool
      for_each_while(std::integer_sequence<I, head, tail...>, Func &&func)
      {
        // can't forward here since we are going to use func later
        return func(std::integral_constant<I, head>())
          && for_each_while(std::integer_sequence<I, tail...>(),
                            std::forward<Func>(func));
      }
    } // namespace ForEachWhileImpl

    //! call a functor, statically passing each item in a sequence, until it returns false
    /**
     * This calls \c func(std::integral_constant<I, i>()) for each integer \c i
     * in \c seq, until the call returns false.
     *
     * \return false if any of the calls returned false, true otherwise
     */
    template<class I, I... i, class Func>
    constexpr bool
    for_each_while(std::integer_sequence<I, i...> seq, Func &&func)
    {
      return ForEachWhileImpl::for_each_while(seq, std::forward<Func>(func));
    }

    namespace ForEachWhileImpl {
      template<class Generator, class Sentinel, class Func>
      constexpr std::enable_if_t<Generator()==Sentinel(), bool>
      for_each_while(std::pair<Generator, Sentinel>, Func &&func)
      {
        return true;
      }

      template<class Generator, class Sentinel, class Func>
      constexpr std::enable_if_t<!(Generator()==Sentinel()), bool>
      for_each_while(std::pair<Generator, Sentinel>, Func &&func)
      {
        using V = std::integral_constant<typename Generator::value_type,
                                         Generator::value>;
        // can't forward here since we are going to use func later
        return func(V())
          && for_each_while(std::pair<typename Generator::next,
                                      next_sentinel<Sentinel> >(),
                            std::forward<Func>(func));
      }
    } // namespace ForEachWhileImpl

    //! call a functor, statically passing each item generated by a generator
    /**
     * This calls \c func(std::integral_constant<I, i>()) for each integer \c
     * i generated by Generator and delimited by Sentinel, until the call
     * returns false.
     *
     * \return false if any of the calls returned false, true otherwise
     */
    template<class Generator, class Sentinel, class Func>
    constexpr bool
    for_each_while(std::pair<Generator, Sentinel> range, Func &&func)
    {
      return ForEachWhileImpl::for_each_while(range, std::forward<Func>(func));
    }

    namespace ForEachImpl {
      template<class Func, class... Indices>
      void for_each(Func &&func, std::tuple<Indices...>)
      {
        std::forward<Func>(func)(Indices()...);
      }

      template<class Func, class... Indices, class Seq0, class... Seqs>
      void for_each(Func &&func, std::tuple<Indices...>, Seq0, Seqs...)
      {
        for_each_while(Seq0(), [&func] (auto in) -> bool {
            // don't forward func -- were using it multiple times
            for_each(func, std::tuple<Indices..., decltype(in)>(), Seqs()...);
            return true;
          });
      }

    } // namespace ForEachImpl

    //! call a functor, statically passing each item in a sequence
    /**
     * This call \c func(std::integral_constant<I, i>()) for each integer \c i
     * in \c seq.
     */
    template<class... Sequences, class Func>
    void for_each(Func &&func)
    {
      ForEachImpl::for_each(std::forward<Func>(func), std::tuple<>(),
                            Sequences()...);
    }

  } // namespace BenchmarkTemplate
} // namespace Dune

#endif // DUNE_BENCHMARK_TEMPLATE_SEQUENCETOOLS_HH
