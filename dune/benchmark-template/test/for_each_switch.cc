#include <config.h>

#include <iostream>
#include <utility>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/benchmark-template/sequencetools.hh>

int main(int argc, char **argv)
{
  using Dune::BenchmarkTemplate::for_each;

  Dune::MPIHelper::instance(argc, argv);

  int one = 0;
  int two = 0;

  for_each<std::index_sequence<1>, std::index_sequence<2> >
    ([&] (auto first, auto second) {
      one = first;
      two = second;
    });

  std::cout << "Got      { " << one << ", " << two << " }\n"
            << "Expected { 1, 2 }" << std::endl;

  return one == 1 && two == 2 ? 0 : 1;
}
